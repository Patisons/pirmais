import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyProgram {
    private static final String FILE_NAME = "text.txt";
    private static final int TABLE_WIDTH = 60;
    private static final int LEFT_TABLE_SIDE_WIDTH;

    static {
        LEFT_TABLE_SIDE_WIDTH = (int) (TABLE_WIDTH * 0.6);
    }

    private static void skipFileEncoding(BufferedReader bufferedReader) throws IOException {
        bufferedReader.readLine();
    }

    private static List<String> readFileTextLines() throws IOException {
        List<String> textLines = new ArrayList<>();
        FileReader fileReader = new FileReader(new File(FILE_NAME));

        try (BufferedReader reader = new BufferedReader(fileReader)) {
            skipFileEncoding(reader);
            String textString = reader.readLine();
            while (textString != null) {
                String[] textLine = textString.split(";");
                Collections.addAll(textLines, textLine);
                textString = reader.readLine();
            }
        }
        return textLines;
    }

    public static void main(String[] args) throws IOException {
        List<String> lines = readFileTextLines();
        int textStringCount = lines.size() / 2;
        symbolLinePrint(TABLE_WIDTH, "*");
        for (int i = 0; i < textStringCount; i++) {
            createAndPrintLine(lines.get(i * 2), lines.get(i * 2 + 1), TABLE_WIDTH, LEFT_TABLE_SIDE_WIDTH);
            if (i < textStringCount - 1) {
                symbolLinePrint(TABLE_WIDTH, "-");
            }
        }
        symbolLinePrint(TABLE_WIDTH, "*");
    }

    private static class CreateLineResult {
        public final String line;
        public final String remaining;

        public CreateLineResult(String line, String remaining) {
            this.line = line;
            this.remaining = remaining;
        }
    }

    private static CreateLineResult createLine(String textLine, int textSideSize) {
        try {
            String l1 = textLine.substring(0, textSideSize - 1);
            int b1 = l1.lastIndexOf(" ");
            l1 = l1.substring(0, b1);
            textLine = textLine.substring(b1 + 1);
            return new CreateLineResult(l1, textLine);
        } catch (Exception e) {
            return new CreateLineResult(textLine, "");
        }
    }

    private static class FormattedLine {
        private final int leftSize;
        private final String leftLine;

        private final int rightSize;
        private final String rightLine;

        public FormattedLine(int leftSize, String leftLine, int rightSize, String rightLine) {
            this.leftSize = leftSize;
            this.leftLine = leftLine;
            this.rightSize = rightSize;
            this.rightLine = rightLine;
        }
        @Override
        public String toString() {
            return String.format("*%-" + (leftSize - 1) + "s", leftLine) +
                    String.format("*%-" + rightSize + "s", rightLine) + "*";
        }
    }

    private static void createAndPrintLine(String leftLine, String rightLine, int tableWidth, int leftSideSize) {
        int leftCL = lineNumber(leftSideSize, leftLine);
        int rightCL = lineNumber(tableWidth-leftSideSize, rightLine);
        int lineCount = rightCL;
        if (leftCL > rightCL) {
            lineCount = leftCL;
        }
        do {
            CreateLineResult leftResult = createLine(leftLine, leftSideSize);
            leftLine = leftResult.remaining;

            CreateLineResult rightResult = createLine(rightLine, tableWidth - leftSideSize);
            rightLine = rightResult.remaining;

            System.out.println(new FormattedLine(leftSideSize, leftResult.line, tableWidth - leftSideSize - 2, rightResult.line));
            lineCount--;
        } while (lineCount > 0);
    }

    static int lineNumber(int tableColummWidth, String textLine) {
        tableColummWidth = tableColummWidth - 2;
        if (textLine.length() <= tableColummWidth) {
            return 1;
        }
        int linesCount = 0;
        do {
            CreateLineResult lineResult = createLine(textLine, tableColummWidth);
            textLine = lineResult.remaining;
            linesCount++;
        } while (textLine.length() > 0);
        return linesCount;
    }

    static void symbolLinePrint(int tableWidth, String symbol) {
        for (int i = 0; i < tableWidth; i++) {
            System.out.print(symbol);
        }
        System.out.println();
    }
}
